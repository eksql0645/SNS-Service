const userModel = require('./user');
const postModel = require('./post');
const tagModel = require('./tag');

module.exports = { userModel, postModel, tagModel };
