const userService = require('./user');
const postService = require('./post');
const refreshService = require('./refresh');
const authService = require('./auth');

module.exports = { userService, refreshService, postService, authService };
